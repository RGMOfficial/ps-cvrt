# ps-cvrt

A port of Denshi's cvrt to Powershell.

`ps-cvrt` is a script for systematically converting media files:
```
./cvrt.ps1 [infiles...] [format out]
```
`[infiles...]` can be any media files (video, audio, images...) while `[format out]` can either be a desired file format, or in the case of converting a single file, a singular filename.

This script also supports setting FFMPEG presets within it, utilizing an external options *.json* file on the script location. Some basic presets are provided as examples.
